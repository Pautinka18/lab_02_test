<?php
require_once("check.php");
if(!$_SESSION){
    header('Location: index.php');
}
else{
    require_once("language.php");
echo $translate[$lang]['Name'].' : '.$_SESSION['name'];
?>
    <br>
    <?php
echo $translate[$lang]['Surname'].' : '.$_SESSION['surname'];?>
    <br>
    <?php
echo $translate[$lang]['Role'].' : '.$_SESSION['role'];
session_destroy();?>
    <br>
    <a href="index.php">Выход из аккаунта</a><br>
<?php
}
?>
