<?php
require_once("check.php");
if($_SESSION['role']=='client'){
    header('HTTP/1.0 403 Forbidden');
}
else {
    require_once("language.php");
    $temp = $_SESSION['role'];
    if ($_POST['profile']) {
        $_SESSION['role'] = $_POST['profile'];
    }
    if ($temp != $_SESSION['role'] && $_POST['profile']) {
        header("Location: index.php");
    }
    echo $translate[$lang]['You can change your role below :'];
    if ($_SESSION['role'] == 'admin' || $_SESSION['role'] == 'manager') {
        ?>
        <form method="POST">
            <select name="profile">
                <option value="admin">admin</option>
                <option value="manager">manager</option>
                <option value="client">client</option>
            </select>
            <input type="submit"/>
        </form>
        <?php
    }
}session_destroy();
?>