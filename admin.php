<?php
require_once('check.php');
if($_SESSION['role']!='admin'){
    header('HTTP/1.0 403 Forbidden');
}
else {

    if ($_POST['lang']) {
        $_SESSION['lang'] = $_POST['lang'];
    }
    if ($_SESSION['lang']) {
        require_once("language.php");
        echo $translate[$lang]['Hello'] . ' ' . $_SESSION['name'] . ' ' . $_SESSION['surname'] . '. ' . $translate[$lang]['You'] . ' ' . $translate[$lang]['can'] . ' ' . $translate[$lang]['change'] . ' ' . $translate[$lang]['language'] . ' ' . $translate[$lang]['below'];
    }
    if (!$_SESSION['lang']) {
        echo 'Change language to continue';
    }
    ?>

    <form method="POST">
        <select name="lang">
            <option value="ru">ru</option>
            <option value="ua">ua</option>
            <option value="it">it</option>
            <option value="en">en</option>
        </select>
        <input type="submit"/>
    </form>
    <?php
    require_once("panel.php");
}
?>